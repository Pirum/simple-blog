<?php

require_once './vendor/autoload.php';
//require_once "autoloader.php";
require_once "class/blogcore.php";
require_once "class/pagination.php";
require_once "class/post.php";
require_once "class/validation.php";

$conf = parse_ini_file("config.ini");
$lang = parse_ini_file($conf['LANG_PATH']);
$db = new PDO('mysql:host='.$conf['DB_HOST'].';dbname='.$conf['DB_NAME'].';charset='.$conf['DB_CHARSET'], $conf['DB_USER'], $conf['DB_PASS']);

/*
** This is the controller file
*/

$action = !empty($_GET['action']) ? $_GET['action'] : 'Index';
require_once 'actions/' . $action . '.php';

$action = new $action($db, $lang);
$data = $action->generate();
$template = $action->getTemplate();

if ($template) {
	$loader = new Twig_Loader_Filesystem('templates');
	$twig = new Twig_Environment($loader);
	echo $twig->render($template, $data);	
}

