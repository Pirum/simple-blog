<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>{{ lang.HEADER_TITLE }}</title>
	<link rel="stylesheet" href="css/style.css">
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body>
	<div class="container">
		<header class="new_post">
			<h1>{{ lang.HOME_TITLE }}</h1>
			<h2>{{ lang.HOME_EXTRA_TEXT }}</h2>
			<form class="form-horizontal" action="index.php?action=insert" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<div class="col-md-8 col-md-offset-2">
						<input class="text" type="text" name="postText" maxlength="255" placeholder="{{ lang.FORM_LABEL_INPUT_NEW_MESSAGE }}">
					</div>
				</div>

				<div class="form-group form-group-sm {% if phoneValidate == "error" %} has-error {% endif %}">
				  <label class="col-md-5 control-label" for="phoneNumber"> {{ lang.FORM_LABEL_INPUT_PHONE_NUMBER }} </label>
				  <div class="col-md-5">
				  	<input type="text" class="form-control text" id="phoneNumber" name="phoneNumber" maxlength="12" placeholder="{{ lang.FORM_PLACEHOLDER_INPUT_PHONE_NUMBER }}">
				  	{% if phoneValidate == "error" %}
				  		<div class="show"><p class="bg-danger text-center"> {{ lang.FORM_VALIDATE_ERR_PHONE_NUMBER }} </p></div>
				  	{% endif %}
				  </div>
				</div>
				
				<div class="form-group form-group-sm">
					<label for="uploadFile" class="col-md-5 control-label"> {{ lang.FORM_LABEL_UPLOAD_FILE }} </label>
					<div class="col-md-7">
						<input type="file" id="uploadFile" name="myFile" accept="image/jpeg">
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<button class="btn btn-success" type="submit"> {{ lang.FORM_SUBMIT }} </button>
					</div>
				</div>
			</form>
		</header>
		
		<div class="display_posts">
		{% for post in posts %}
			<div class="row">
				<div class="col-md-1">
					<p class="text-muted text-center">{{ lang.POST_PUBTIME }} <br/>{{ post.pubTime }} </p>
				</div>
				<div class="col-md-2">
					<img class="post_img" src="img/{% if post.imgFileName == null %}noimage.png {% else %}{{ post.imgFileName }} {% endif %}" />
				</div>
				<div class="col-md-8">
					<p class="post_text">{{ post.text }}</p>
				</div>
				<div class="col-md-1">
					<form action="index.php" method="get">
					<label class="post_delete">
						<input type="hidden" name="action" value="Delete">
						<input type="hidden" name="postId" value="{{ post.id }}">
						<input type="hidden" name="p" value="{{ pagination.getCurrentPage() }}">
						<button type="submit" class="close" aria-label="{{ lang.POST_DELETE }}" title="{{ lang.POST_DELETE }}"><span aria-hidden="true">&times;</span></button>
					</label>
					</form>
				</div>
			</div>
		{% endfor %}
		</div>
		
		<div class="pagination">
			<p class="text-center">{{ [lang.PAGINATION_CURRENT_PAGE, pagination.getCurrentPage() + 1] | join(' ') }} / {{ pagination.numPages() }}</p> 
			
			{% if pagination.getCurrentPage() > 0 %}
				<a href="index.php?p={{ pagination.prevPage }}" class="btn btn-primary active btn-xs" role="button" > {{ lang.PAGINATION_PREV_PAGE }} </a>
			{% endif %}
			
			{% if pagination.getCurrentPage() < (pagination.numPages() - 1) %}
				<a href="index.php?p={{ pagination.nextPage }}" class="btn btn-primary active btn-xs" role="button" > {{ lang.PAGINATION_NEXT_PAGE }} </a>
			{% endif %}
		</div>
		
		<footer>
			<p class="text-center">&copy; {{lang.FOOTER_COPYRIGHT_BY}}</p>
		</footer>
	</div>
</body>
</html>