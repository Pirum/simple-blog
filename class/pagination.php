<?php

class Pagination {
	private $page;
	private $limit;
	public 	$numRows;
	public 	$numPages;
	
	// konstruktor może być tylko jeden
	public function __construct($page = 0) {
		$this->page = $page;
	}
	public function setNumRows($numRows) {
		$this->numRows = $numRows;
	}
	
	public function setLimit($limit) {
		$this->limit = $limit;
	}
	
	// get number of current page
	public function getCurrentPage() {
		// nie chcemy aby klasa miała łączność ze światem zewnętrznym
		// więc stworzyliśmy publiczną funkcję __construct($page = 0)
		// wywołując nową klasę, przekazujemy parametr ze świata zewnętrznego: $pagination = new Pagination($_GET['p']);
		return $this->page;
	}
	
	// generate link for next page
	public function nextPage() {
		//if ($this->numPages($this->numRows, $this->limit) > ($this->getCurrentPage() + 1) ) {
			return ((int) $this->getCurrentPage() + 1);
		//}
		//return false;
	}
	
	// generate link for previous page
	public function prevPage() {
		if ($this->getCurrentPage() == 0) {
			return false;
		}
		return ((int) $this->getCurrentPage() - 1);
	}
	
	// calculate how many pages we can display from all of records in database
	public function numPages() {
		return ceil($this->numRows / $this->limit);
	}
	
	// prepare additional SQL statement for pagination
	public function sqlPagination() {
		return $this->limit;
	}
	
	public function sqlPaginationOffset($limit) {
		return $limit * $this->getCurrentPage();
	}
}

