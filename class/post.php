<?php

class Post {
	public $text;
	public $imgFileName;
	public $publishedTime;
	public $id;
	
	public function getMessage() {
		return $this->text;
	}
	
	public function getImg() {
		if (!file_exists("img/".$this->imgFileName)) {
			return "noimage.png";
		}
		
		return $this->imgFileName;
	}
	
	public function getPubTime() {
		return $this->publishedTime;
	}
	
	public function getPostId() {
		return $this->id;
	}
}
