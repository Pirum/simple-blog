<?php

class BlogCore {
	private $db;
	
	public function __construct($db) {
		$this->db = $db;
	}
	
	public function insert($postText, $imgFileName = "noimage.png") {
		// prepare SQL statement for insert values
		$sql = "INSERT INTO posts (text, imgFileName, publishedTime) VALUES (
								:postText,
								:imgFileName,
								NOW()
								)";
				
								
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':postText', $postText);
		$stmt->bindParam(':imgFileName', $imgFileName);
		$result = $stmt->execute();
		if ($result) {
			return true;
		}
		
		return false;
	}
	
	public function delete($id = NULL) {
		// prepare statement for delete value
		if (!is_null($id) && is_numeric($id)) {
			$sql = "DELETE FROM posts WHERE id=:id";
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam(':id', $id);
			if (!$stmt->execute()) {
				return false;
			}
			
			return true;
		}
	}
	
	public function select($limit, $offset) {
		$sql = "SELECT * FROM posts ORDER BY publishedTime DESC LIMIT :limit OFFSET :offset";
		$stmt = $this->db->prepare($sql); 
		$stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
		$stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
		
		if (!$stmt->execute()) {
			return false;
		}
		
		return $stmt->fetchAll(PDO::FETCH_CLASS, "Post");
	}
	
	public function getNumRows() {
		$stmt = $this->db->query('SELECT COUNT(*) FROM posts');
		$result = $stmt->fetchColumn();
		if (!$result) {
			return false;
		}
		
		return $result;
	}
	
	// function to upload files
	public function saveFile($myFile) {
		if (empty($myFile)) {
			return false;
		}
		
	    if ($myFile["error"] !== UPLOAD_ERR_OK) {
	        echo "<p>An error occurred.</p>";
	        return false;
	    }
	    
	    $fileName = "/img/";
	    $fileName .= $myFile["name"];
	    
	    // preserve file from temporary directory
	    $dest = dirname(__FILE__, 2);
	    $success = move_uploaded_file($myFile["tmp_name"], $dest . $fileName);
	    if (!$success) { 
	        echo "<p>Unable to save file FROM: " . $myFile["tmp_name"] . " TO: " . $dest . $fileName . "</p>";
	        return false;
	    }
	    
	    return true;
		
	}
}
