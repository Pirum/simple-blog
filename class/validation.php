<?php

class Validation {
	private $phoneNum;
	
	public function phoneNum($phoneNum) {
		$regexp = "/(\+\d\d)?\d{3} ?\d{3} ?\d{3}/";
		if (preg_match($regexp, $phoneNum)) {
			return true;
		}
		
		return false;
	}
}