<?php

class Delete {
	private $id;
	private $lang;
	private $db;
	private $actualPage;
	
	public function __construct($db, $lang) {
		$this->lang = $lang;
		$this->db = $db;
	}
	
	public function generate() {

		// new Core object
		$core = new BlogCore($this->db);

		// prepare statement for delete value
		$id 		= $_GET['postId'];
		$actualPage = $_GET['p'];
		
		if (!empty($id)) {
			$core->delete($id);
		}
		
		header("Location: /codeme/06_blog_v5_mvc/index.php?p=" . $actualPage);
		return [];
	}
	
	public function getTemplate() {
		return null;
	}
}