<?php

class Index {
	private $lang;
	private $db;
	
	public function __construct($db, $lang) {
		$this->lang = $lang;
		$this->db = $db;
	}
	
	public function generate() {
		
		// new Core object 
		$core = new BlogCore($this->db);
		
		/*
		** pagination
		*/
		$numRows = $core->getNumRows();
		$pagination = new Pagination($_GET['p']);
		$pagination->setNumRows($numRows);
		$pagination->setLimit(3); // set 3 posts on one page
		$limit = $pagination->sqlPagination();
		$offset = $pagination->sqlPaginationOffset($limit);
		
		/*
		** select detalis form database
		*/
		$posts = $core->select($limit, $offset);
		
		return [
				"posts" 		=> $posts,
				"pagination" 	=> $pagination,
				"lang" 			=> $this->lang
				];
	}
	
	public function getTemplate() {
		return "default.tpl";
	}
}