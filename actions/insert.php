<?php

class Insert {
	private $lang;
	private $db;
	private $postText;
	private $fileName;
	private $myFile;
	private $phoneValidate;
	
	public function __construct($db, $lang) {
		$this->lang = $lang;
		$this->db = $db;
	}
	
	public function generate() {
		
		$postText 	= $_POST["postText"];
		$myFile		= $_FILES["myFile"];
		$fileName	= $myFile["name"];
		
		/*
		** phoneValidate for form validation
		*/
		$phoneValidate = null;
		$phoneNum = $_POST['phoneNumber'];
		if (!empty($phoneNum)) {
			$validate = new Validation;
			if (!$validate->phoneNum($phoneNum)) {
				$phoneValidate = "error";
			} else {
				$phoneValidate = "ok";	
			}
		}
		
		// new Core object
		$core = new BlogCore($this->db);

		// do insert in SQL
		if (!empty($postText)) {
			$core->insert($postText, $fileName);
			$core->saveFile($myFile);
		}
		
		header("Location: /codeme/06_blog_v5_mvc/index.php");
		return ["phoneValidate"	=> $phoneValidate];
	}
	
	public function getTemplate() {
		return null;
	}
}