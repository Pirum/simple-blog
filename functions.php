<?php

// autoload all class files
//require_once "autoloader.php";

// variables from forms
$postText 	= $_POST["postText"];
$phoneNum	= $_POST["phoneNumber"];
$myFile		= $_FILES["myFile"];
$fileName	= $myFile["name"];
$id 		= $_GET["postId"];

// create new Object with SQL connection and methods
$core 		= new BlogCore($conf['DB_HOST'], $conf['DB_NAME'], $conf['DB_CHARSET'], $conf['DB_USER'], $conf['DB_PASS']);

// do insert in SQL
if (!empty($postText)) {
	$core->insert($postText, $fileName);
	$core->saveFile($myFile);
}

$sysMsg = true;
if (!empty($phoneNum)) {
	$validate = new Validation;
	if (!$validate->phoneNum($phoneNum)) {
		$sysMsg = false;
	} else {
		$sysMsg = true;	
	}
}

// prepare statement for delete value
if (!empty($id)) {
	$core->delete($id);
}

/*
** pagination
*/
$numRows = $core->getNumRows();
$pagination = new Pagination($_GET['p']);
$pagination->setNumRows($numRows);
$pagination->setLimit(3); // set 3 posts on one page
$limit = $pagination->sqlPagination();
$offset = $pagination->sqlPaginationOffset($limit);

/*
** select detalis form database
*/
$posts = $core->select($limit, $offset);
